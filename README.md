## BSFG: Blender Shaders for Godot

## Description
Blender shaders ported to godot v4.

## Installation
> NOTE: Godot4's relative paths are broken for shader include. So keep this project in root dir of project. e.g. `res://bsfg/`. Refer to this [issue](https://github.com/godotengine/godot/issues/67811)

Clone this repository inside your godot v4 project. 

## Usage
> NOTE: Godot4's relative paths are broken for shader include. So only use absolute paths for shader include like shown below. Refer to this [issue](https://github.com/godotengine/godot/issues/67811)

Include shader files into your shader like:
```
#include "res://bsfg/path/to/file.gdshaderinc"
```

## License
Under multiple licenses like rust.

See [LICENSE-APACHE](LICENSE-APACHE), [LICENSE-MIT](LICENSE-MIT), and
[COPYRIGHT](COPYRIGHT) for details.

## Project status
In development.
